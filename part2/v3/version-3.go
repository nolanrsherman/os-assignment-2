/**
 * This program is v2 of Part 2 for
 * OS assignment 2. It allows 2 threads
 * for the analyzing of a text file.
 * Author: Nolan Sherman
 */
package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"time"
)

//scan scans the file and adds the lines to
//the buffered channel. This is a thread
//that exclusively handles file I/O
func scan(file *os.File, cLines chan string) {
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		cLines <- scanner.Text()
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	close(cLines)

}

//analyze receives lines from the channel and
//processes them until the channel is closed.
func analyze(count []int, cLines chan string) {
	for line := range cLines {
		regxDash := regexp.MustCompile("-")
		regxNotWords := regexp.MustCompile("\\W")
		line = regxDash.ReplaceAllString(line, "_")     //convert - to _
		line = regxNotWords.ReplaceAllString(line, " ") //replace all non word characters with space
		line = strings.TrimSpace(line)
		words := strings.Split(line, " ")
		for _, word := range words {
			if len(word) == 0 {
				continue
			} //ignore white spaces
			i := len(word) - 1
			if i > 7 {
				i = 7
			}
			count[i]++
		}
	}
}

func main() {
	start := time.Now()
	filepath := parseFlags() //parse flags and return the filepath
	//filepath:= "part2/enwik9"
	file, err := os.Open(*filepath)
	if err != nil {
		log.Fatal(err)
	}
	count := []int{0, 0, 0, 0, 0, 0, 0, 0}
	defer file.Close()

	cLine := make(chan string, 10000)
	go scan(file, cLine)     //File Input on new thread (thread 2)
	go analyze(count, cLine) //line processing in new thread (thread 3)
	analyze(count, cLine)    //line processing on current thread (thread 1)

	resultTemplate := `--- Results --- 
	Length		Count
	1		%d
	2		%d
	3		%d
	4		%d
	5		%d
	6		%d
	7		%d
	>7		%d`
	log.Printf(resultTemplate, count[0], count[1], count[2], count[3], count[4], count[5], count[6], count[7])
	end := time.Now()
	duration := end.Sub(start)
	fmt.Printf("This took %v\n", duration)
}

func parseFlags() (filepath *string) {
	filepath = flag.String("file", "", "The path to the file you want to analyze")
	showHelp := flag.Bool("help", false, "Prints the full help text to stdout")
	flag.Parse()
	if *showHelp {
		text := `--- Text File Analyzer ---- 
		Reads a text file and finds the frequency of each word length from 1 to 7, and all words greater then 7.
		
		Flags (*Required)
			--file=<TEXTFILE>  *		The file you would like to analyze`
		log.Printf(text)
		os.Exit(0)
	}
	if *filepath == "" {
		log.Fatal("flag --file=<filepath> is a required flag.")
	}
	return
}
