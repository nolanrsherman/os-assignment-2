/**
* This file is the entry point for my
* solution to Part 1 of Assignment 2
* Author: Nolan Sherman
 */
package main

import (
	"fmt"
	"math"
	"time"
)

var n = 69881631850817231

func main() {

	start := time.Now()

	isPrime := true
	sqOfN := int(math.Floor(math.Sqrt(float64(n))))
	for i := 2; i <= sqOfN; i++ {
		if n%i == 0 {
			isPrime = false
			break
		}
	}
	if isPrime {
		fmt.Printf("%d is a prime number\n", n)
	} else {
		fmt.Printf("%d is NOT a prime number\n", n)
	}

	end := time.Now()
	duration := end.Sub(start)
	fmt.Printf("This took %v\n", duration)

}
