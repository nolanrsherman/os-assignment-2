/**
* This file is the entry point for my
* solution v2 to Part 1 of Assignment 2
* Author: Nolan Sherman
 */
package main

import (
	"fmt"
	"math"
	"time"
)

var n = 69881631850817231

func main() {

	start := time.Now()
	sqOfN := int(math.Ceil(math.Sqrt(float64(n))))

	c := make(chan bool, 3) //Chanel to store thread resutls

	go primeSearchThread(n, 2, sqOfN/3, c)               //thread A (2 to 1/3)
	go primeSearchThread(n, (sqOfN / 3), (sqOfN/3)*2, c) //thread B (1/3 to 2/3)
	go primeSearchThread(n, (sqOfN/3)*2, sqOfN, c)       //thread C (2/3 to 1)

	tA := <-c //result 1
	tB := <-c //result 2
	tC := <-c //result 3

	if tA && tB && tC { //if all search space parts return true
		fmt.Printf("%d is a prime number\n", n)
	} else {
		fmt.Printf("%d is NOT a prime number\n", n)
	}

	end := time.Now()
	duration := end.Sub(start)
	fmt.Printf("This took %v\n", duration)

}

func primeSearchThread(n, start, end int, c chan bool) {
	c <- isPrimeSearch(n, start, end)
}

func isPrimeSearch(n, start, end int) bool {
	isPrime := true
	for i := start; i <= end; i++ {
		if n%i == 0 {
			isPrime = false
			break
		}
	}
	return isPrime
}
