# OS Assignment 2
By Nolan Sherman


# Part 1

For v1 and v2 of Part 1, I used GO to implemement the prime number caclulator. In version 2 I create a channel with a buffer
size of 3, split the search space into 3 equal parts and run each part in a seperate go routine. When all routines
have pushed a boolean value to the channel, I check if all results were true. This indicates that the number is indeed a prime (or not).

## Results

**v1** - *Unthreaded* [./part1/v1/version-1.go](./part1/v1/version-1.go)


**v2** - *Threaded* [./part1/v2/version-2.go](./part1/v2/version-2.go)

For the Results below, I ran **v1** and **v2** with a different number of system level threads (cores) available. This was achieved my using the GO System Variable GOMAXPROCS:
```bash
> export GOMAXPROCS=<NumberOfCores>
> go run ./part1/v1/version-2.go
```


|       | v1 1-Core  | v2 1-Core  | v2 2-Core  | v2 3-Core  |
|:-----:|:----------:|:----------:|:----------:|:----------:|
| Run 1 |2.131866507s|2.146848420s|1.445552397s|0.816878236s|
| Run 2 |2.134709783s|2.177622105s|1.459156079s|0.814417472s|
| Run 3 |2.154445707s|2.146520624s|1.421215662s|0.807662140s|
| Run 4 |2.148658112s|2.146520624s|1.449211763s|0.807938090s|
| Run 5 |2.203687117s|2.146520624s|1.502063651s|0.802117549s|
| Mean  |2.154673445s|2.152806479s|1.4554399104|0.809802697s|
| Median|2.148658112s|2.146520624s|1.4492117630|0.807938090s|

## Comments
As we can see from the results above, as the available cores increases, the time 
it takes for the program to complete decreases. This is because when more threads
are utilized by out program, the workload can execute in parallel (for a multi-core machine).
We see that v1 on 1 thread, and v2 on 1 thread have similar execution times. This is expected. 
Even though v2 is coded to run on multiple threads, if it is confined to using only one thread
then we should expect that it will have a similar run time to the single threaded v1.

# Part 2
For part 2 we were tasked with creating a program that calculates the number
of n length words in a file where n is = 1...7 and where n>7. There are 3 files
that comprise my solutions.

1. **v1** - *Untreaded* [./part2/v1/version-1.go](./part2/v1/version-1.go)
2. **v2** - *2 Threads* [./part2/v2/version-2.go](./part2/v2/version-2.go)
3. **v3** - *Three Threads* [./part2/v3/version-3.go](./part2/v3/version-3.go)

I used ```head -n 6000000 enwik9 > enwik9-short.txt``` to pair the source input file down to 
a size that runs in v1 for a little over 1 minute. out put results for the 3 versions looks like:
```
v1                      
Length  Count              
1   	3837909 	 	 
2   	12485826		
3   	12182424		
4   	10575586		
5   	7747423 	 	 
6   	6269384 	 	 
7   	5808396 	 	 
>7  	13314158		

v2
Length  Count
1   	3837909
2   	12485826
3   	12182424
4   	10575586
5   	7747423
6   	6269384
7   	5808396
>7  	13314158


v3          
Length  Count
1   	3837909
2   	12485826
3   	12182424
4   	10575586
5   	7747423
6   	6269384
7   	5808396
>7  	13314158
```

## Results

For the Results below, I ran **v1**, **v2** and **v3** with a different number of system level threads (cores) available. This was achieved my using the GO System Variable GOMAXPROCS:
```bash
> export GOMAXPROCS=<NumberOfCores>
> go run ./part2/v1/version-1.go --file ./enwik9-short.txt
> go run ./part2/v2/version-2.go --file ./enwik9-short.txt
> go run ./part2/v3/version-3.go --file ./enwik9-short.txt
```

Below are my time results for running the 3 different versions with the specific configurations.

|       | v1 1-Core     | v2 1-Core    | v2 2-Core    | v3 3-Core   |
|:-----:|:-------------:|:------------:|:------------:|:-----------:|
| Run 1 |78.392473636s  |69.870670908s |79.769673782s |45.293168557s|
| Run 2 |79.195865440s  |60.599245196s |79.278380668s |45.607642032s|
| Run 3 |80.002948906s  |61.582089191s |81.948040399s |45.192067423s|
| Run 4 |79.633675429s  |63.396047665s |79.222625435s |45.060808285s|
| Run 5 |78.551540297s  |63.857321264s |80.297206057s |45.510032645s|
| Mean  |79.155300741s  |63.861074844s |80.103185268s |45.332743788s|
| Median|79.195865440s  |63.396047665s |79.769673782s |45.293168557s|

## Coments
As we see from the results table above, completion time of this program generally decreases
as the number of execution threads increases. We see an anomaly between v1 with 1-core and v2 with
1-core. The execution time decreases slightly. I am hypothesizing that the restructing of the code
form v1 to v2, and the use of GO internal concurrency structures must have gave the algorithm
a small boost in efficiency, even though they are both running on one thread. Also, we see that running
v2 on two cores suprisingly increases the execution time. Again, I hypothesize this being due to 
the implementation and how I used the GO concurrency structures to model this problem. But you will see
that once we allow for 3 execution threads in v3, the execution time is nearly halved. I think this indicates
that the a lot of the bottle execution time bottleneck on my machine was coming from the cpu bound tasks. Once
to worker threads were dedicated to parsing the text, execution time dropped dramaticaly.

# Final Comments
It is clear from looking at both parts 1 and 2 that execution time decreases in general with
the amount of threads available to tackle the problem. But concurrency is more complex then this as
revealed by part 2. Its true, if you can parallelize cpu bound tasks decresing execution time is simple. 
However, when combining cpu bound tasks with blocking calls that are made for I/O, Network, etc... 
the problem is slightly more complex and more threads is not the most accurate answer. A combination
of the write concurrency model/algorithm and the optimized amount of threads for the right work (cpu vs blocking)
is a much better approach.